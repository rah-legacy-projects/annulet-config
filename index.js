module.exports = exports = {
	configuration: require("./configuration"),
	middleware: require("./middleware"),
	databaseLists:{
		automatedTest: require('./databaseLists/databaseList-automatedTest'),
		dev: require('./databaseLists/databaseList-dev'),
		staging: require('./databaseLists/databaseList-staging'),
		prod: require('./databaseLists/databaseList-dev')
	}
};
