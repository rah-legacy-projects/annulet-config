var logger = require('winston'),
    paths = require('../configuration/paths'),
    async = require('async'),
    _ = require('lodash'),
    util = require('util'),
    paths = require('../configuration/paths');
require('annulet-util');

module.exports = exports = {
    client: function(req, res, next) {
        res.locals.clientAppRequireConfig = paths.clientAppRequireConfig();
        next();
    },
    signup: function(req, res, next) {
        res.locals.signupAppRequireConfig = paths.signupAppRequireConfig();
        next();
    }
}
