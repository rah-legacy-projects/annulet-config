var request = require('request'),
    logger = require('winston'),
    util = require('util'),
    os = require('os');
    paths = require('../configuration/paths');
module.exports = exports = function(x, cb) {
    logger.error('UNCAUGHT: ' + x);
    logger.error(x.stack);
    request.post({
        url: paths.apiUri() + '/problem',
        headers: {},
        body: {
            environment: process.env.ENV,
            server: {
                hostname: os.hostname(),
                type: os.type(),
                platform: os.platform(),
                arch: os.arch(),
                release: os.release(),
                uptime: os.uptime(),
                loadavg: os.loadavg(),
                freemem: os.freemem(),
                totalmem: os.totalmem(),
            },
            exception: util.inspect(x, null, 10),
            customer: '[uncaught exception, no customer]',
            userAgent: '[uncaught exception, no user agent]',
            createdBy: '[uncaught exception]',
            modifiedBy: '[uncaught exception]'
        },
        json: true
    }, function(err, response, body) {
        if (!!err) {
            logger.error('!!! ERROR IN UNCAUGHT EXCEPTION TRACKING: ' + util.inspect(err));
        }
        if (!!cb) {
            cb(err);
        }
    });
}
