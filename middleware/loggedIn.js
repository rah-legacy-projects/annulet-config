var logger = require('winston'),
    paths = require('../configuration/paths'),
    util = require('util'),
    request = require('request');
module.exports = exports = function(req, res, next) {
    //get the auth token from the headers
    var authToken = req.get('annulet-auth-token') || req.query['access_token'];
    var internal = req.get('annulet-internal');
    req.token = authToken;
    if (!process.env.USEAUTH || process.env.USEAUTH == 'true') {
        if (!authToken && !internal) {
            logger.warn('no auth provided');
            res.status(403)
                .json({
                    message: 'no auth provided'
                });
        } else if (!!authToken) {
            //issue a request to the auth service to validate token
            var authRequestEndpoint = paths.authUri() + '/getUserInfo?access_token=' + authToken;
            request({
                url: authRequestEndpoint,
                headers: {
                    'user-agent': req.headers['user-agent'] || '[undefined]'
                }
            }, function(err, response, body) {
                if (response.statusCode == 200) {
                    req.loggedInUser = JSON.parse(body)
                        .data;
                    next();
                } else {
                    res.status(response.statusCode)
                        .end();
                }
            });
        } else if (!!internal) {
            if (paths.internalKey() == internal) {
                next();
            } else {
                res.status(403)
                    .end();
            }
        }
    } else {
        logger.warn('authorization is turned off.');
        next();
    }
};
