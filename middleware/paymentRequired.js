var logger = require('winston'),
    paths = require('../configuration/paths'),
    request = require('request'),
    async = require('async'),
    _ = require('lodash'),
    util = require('util'),
    stripe = require('stripe')(require('../configuration/stripe')
        .privateKey()),
    paths = require('../configuration/paths');
require('annulet-util');

module.exports = exports = function(req, res, next) {
    var customer = req.get('annulet-auth-customer') || req.query['annulet_auth_customer'],
        internal = req.get('annulet-internal') || req.query['annulet_internal'];
    logger.silly('[payment reqd] customer: ' + customer);
    if (!internal) {
        async.waterfall([

            function(cb) {
                //get internal customer
                request.get({
                    url: paths.apiUri() + '/customer/' + customer,
                    headers: {
                        'annulet-internal': paths.internalKey(),
                        'user-agent': req.headers['user-agent'] || '[undefined]'
                    },
                    json: true
                }, function(err, response, responseBody) {
                    cb(err, {
                        customer: responseBody.data
                    });
                });
            },
            function(p, cb) {
                //get stripe customer
                if (!!p.customer) {
                    stripe.customers.retrieve(p.customer.stripeCustomerId, function(err, stripeCustomer) {

                        p.stripeCustomer = stripeCustomer;
                        if (!!err && !!err.type && err.type == 'StripeConnectionError') {
                            //if we can't connect to Stripe, allow requests through.
                            //lack of connectivity to Stripe shouldn't disallow customers to use Annulet,
                            //even if they are not up to date on payment.
                            cb(null, p);
                        } else {
                            cb(err, p);
                        }
                    });
                } else {
                    cb(null, p);
                }
            }
        ], function(err, p) {
            if (!!err) {
                logger.error('[payment reqd] error: ' + util.inspect(err));
                res.status(500)
                    .json({
                        err: err
                    });
            } else if (!p.customer) {
                logger.error('[payment reqd] no customer for ' + customer);
                res.status(404)
                    .json({
                        err: {
                            message: 'Customer not found!'
                        }
                    });
            } else if (!p.stripeCustomer) {
                logger.error('[payment reqd] no stripe customer for ' + customer);
                res.status(404)
                    .json({
                        err: {
                            message: 'Stripe customer not found!'
                        }
                    });
            } else if (p.stripeCustomer.delinquent) {
                logger.warn('[payment reqd] customer delinquent: ' + customer);
                res.status(402)
                    .end();
            } else {
                next();
            }

        });
    } else {
        if (paths.internalKey() == internal) {
            req.customer = customer;
            next();
        } else {
            res.status(403)
                .end();
        }
    }
};
