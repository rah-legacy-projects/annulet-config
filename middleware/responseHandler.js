var logger = require('winston'),
    paths = require('../configuration/paths'),
    request = require('request'),
    util = require('util'),
    os = require('os'),
    _ = require('lodash');

module.exports = exports = function() {
    var err, req, res, next;
    if (arguments.length == 4) {
        err = arguments[0];
        req = arguments[1];
        res = arguments[2];
        next = arguments[3];
    } else if (arguments.length == 3) {
        req = arguments[0];
        res = arguments[1];
        next = arguments[2];
    } else {
        req = arguments[0];
        res = arguments[1];
    }

    var makeException = function(err, cb) {
        logger.error('[response handler] exception: ' + util.inspect(err, null, 10));
        var customer = req.get('annulet-auth-customer') || req.query['annulet_auth_customer'];
        var authToken = req.get('annulet-auth-token') || req.query['access_token'];
        var who = ((!!req.loggedInUser ? req.loggedInUser.email : null) || authToken) || 'NOT SPECIFIED';
        request.post({
            url: paths.apiUri() + '/problem',
            headers: {},
            body: {
                environment: process.env.ENV,
                server: {
                    hostname: os.hostname(),
                    type: os.type(),
                    platform: os.platform(),
                    arch: os.arch(),
                    release: os.release(),
                    uptime: os.uptime(),
                    loadavg: os.loadavg(),
                    freemem: os.freemem(),
                    totalmem: os.totalmem(),
                },
                exception: util.inspect(err, null, 10),
                customer: customer,
                userAgent: req.useragent,
                createdBy: who,
                modifiedBy: who
            },
            json: true
        }, function(err, response, body) {
            if (!!err) {
                logger.error("!!! ERROR IN EXCEPTION TRACKING: " + util.inspect(err));
            }
            cb(err, body.ticketNumber);
        });
    };

    var status = 200;
    if (!!res.statusOverride) {
        status = res.statusOverride;
    } else if (!!err || !!res.err) {
        status = 500;
    }

    if (!!err) {
        makeException(err, function(err, ticketNumber) {
            res.status(status)
                .json({
                    err: {
                        message: err.message,
                        ticketNumber: ticketNumber
                    },
                    fatal: true
                });
        });
    } else if (!!res.err) {
        makeException(res.err, function(err, ticketNumber) {
            res.status(status)
                .json({
                    err: {
                        message: res.err.message,
                        ticketNumber: ticketNumber
                    },
                    fatal: (res.err.fatal || false)
                });
        });
    } else if (/^[45]\d{2}/.test(status)) {
        makeException(status + ' problem, no error associated', function(err, ticketNumber) {
            res.status(status)
                .json({
                    err: {
                        ticketNumber: ticketNumber
                    },
                    data: res.data
                });
        });
    }
    /*else if (!!next) {
        //cannot do this unless a way is determined if next() is the 404 middleware from express
        logger.silly('next?');
        next();
    }*/
    else {
        res.status(status)
            .json({
                data: res.data
            });
    }
};
