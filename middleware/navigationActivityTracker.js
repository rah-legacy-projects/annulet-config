var logger = require('winston'),
    paths = require('../configuration/paths'),
    request = require('request'),
    _ = require('lodash');
module.exports = exports = function() {
    var err, req, res, next;
    if (arguments.length == 4) {
        err = arguments[0];
        req = arguments[1];
        res = arguments[2];
        next = arguments[3];
    } else if (arguments.length == 3) {
        req = arguments[0];
        res = arguments[1];
        next = arguments[2];
    } else {
        req = arguments[0];
        res = arguments[1];
    }

    var customer = req.get('annulet-auth-customer') || req.query['annulet_auth_customer'];
    var authToken = req.get('annulet-auth-token') || req.query['access_token'];
    var who = ((!!req.loggedInUser ? req.loggedInUser.email : null) || authToken) || 'NOT SPECIFIED';
    logger.silly('[nav activity] customer: ' + customer);
    logger.silly('[nav activity] authtoken: ' + authToken);
    logger.silly('[nav activity] who: ' + who);
    request.post({
        url: paths.apiUri() + '/navigationActivity',
        //headers: {},
        body: {
            customer: customer,
            userAgent: req.useragent,
            requestedBy: who,
            verb: req.method,
            query: req.query,
            params: req.params,
            body: req.body,
            headers: req.headers,
            path: req.url
        },
        json: true
    }, function(err, response, body) {
        if (!!err) {
            logger.error("!!! ERROR IN ACTIVITY TRACKING: " + util.inspect(err));
        }

        next();
    });
};
