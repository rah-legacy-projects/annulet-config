var logger = require('winston'),
    paths = require('../configuration/paths'),
    request = require('request'),
    _ = require('lodash');
module.exports = exports = function(req, res, next) {
    //get the auth token from the headers
    var internal = req.get('annulet-internal') || req.query['annulet_internal'];
    if (!process.env.USEAUTH || process.env.USEAUTH == 'true') {
        if (paths.internalKey() == internal) {
            next();
        } else {
            res.status(403)
                .end();
        }
    } else {
        //todo: set up the customer anyway
        logger.warn('authorization is turned off.');
        next();
    }
};
