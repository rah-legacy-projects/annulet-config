var logger = require('winston'),
    paths = require('../configuration/paths'),
    request = require('request'),
    _ = require('lodash');
module.exports = exports = function(req, res, next) {
    //get the auth token from the headers
    var authToken = req.get('annulet-auth-token') || req.query['access_token'];
    var customer = req.get('annulet-auth-customer') || req.query['annulet_auth_customer'];
    var internal = req.get('annulet-internal') || req.query['annulet_internal'];
    req.token = authToken;
    if (!process.env.USEAUTH || process.env.USEAUTH == 'true') {
        //issue a request to the auth service to validate token

        if (!internal) {
            var apiRequestEndpoint = paths.apiUri() + '/customer?access_token=' + authToken;
            request({
                url: apiRequestEndpoint,
                headers: {
                    'user-agent': req.headers['user-agent'] || '[undefined]'
                }
            }, function(err, response, body) {
                if (response.statusCode == 200) {
                    //verify the customer requested is in the access listing for the user
                    var customers = JSON.parse(body)
                        .data;
                    var customerObj = _.find(customers, function(c) {
                        logger.silly("\t\t " + c.id + ' == ' + customer);
                        return c.id == customer;
                    });

                    if (!!customerObj) {
                        req.customer = customer;
                        next();
                    } else {
                        logger.warn('[customer valid] customer not found!');
                        res.status(403)
                            .end();
                    }
                } else {
                    logger.error('[customer valid] customer not valid');
                    res.status(response.statusCode)
                        .end();
                }
            });
        } else {
            if (paths.internalKey() == internal) {
                req.customer = customer;
                next();
            } else {
                res.status(403)
                    .end();
            }
        }
    } else {
        //todo: set up the customer anyway
        logger.warn('authorization is turned off.');
        next();
    }
};
