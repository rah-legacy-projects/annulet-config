var stripeConfig = {};
stripeConfig.privateKey = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for credit card processing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        //return 'sk_live_AOUUJHVyzY93NjatWoaDBBz6';
        return 'sk_test_T6jtj6LYmypP4UzCHZyvkptx';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'sk_test_TOpet1VFr5cxjEqw1bvpN4Zb';
    } else if (/development/i.test(process.env.ENV)) {
        return 'sk_test_TOpet1VFr5cxjEqw1bvpN4Zb';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'sk_test_TOpet1VFr5cxjEqw1bvpN4Zb';
    }
};

stripeConfig.publicKey = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for credit card processing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        //return 'pk_live_2kbIgh0ZO9j4Vvz6AEvtHhFa';
        return 'pk_test_h6hT3cmyxGGkgXak8MRChxDk';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'pk_test_U9CjPQcjUb6HKJ7FVUQCgc4x';
    } else if (/development/i.test(process.env.ENV)) {
        return 'pk_test_U9CjPQcjUb6HKJ7FVUQCgc4x';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'pk_test_U9CjPQcjUb6HKJ7FVUQCgc4x';
    }
};

stripeConfig.sampleCustomerId = function(){
   if (!process.env.ENV) {
        throw new Error("Environment must be defined for credit card processing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return '';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'cus_5lIaShPYbBlMRk';
    } else if (/development/i.test(process.env.ENV)) {
        return 'cus_5lIaShPYbBlMRk';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'cus_5lIaShPYbBlMRk';
    }
};
module.exports = exports = stripeConfig;
