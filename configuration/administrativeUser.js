var adminUser = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for ports to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return {
            firstName: 'Production',
            lastName: 'System Administrator',
            email: 'prod_sysadmin@annulet.io',
            password: 'password',
            access: ['user', 'owner', 'administrator']
        };
    } else if (/staging/i.test(process.env.ENV)) {
        return {
            firstName: 'Staging',
            lastName: 'System Administrator',
            email: 'staging_sysadmin@annulet.io',
            password: 'password',
            access: ['user', 'owner', 'administrator']
        };
    } else if (/development/i.test(process.env.ENV)) {
        return {
            firstName: 'Development',
            lastName: 'System Administrator',
            email: 'dev_sysadmin@annulet.io',
            password: 'password',
            access: ['user', 'owner', 'administrator']
        };
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return {
            firstName: 'Autotest',
            lastName: 'System Administrator',
            email: 'at_sysadmin@annulet.io',
            password: 'password',
            access: ['user', 'owner', 'administrator']
        };
    }
};

module.exports = exports = adminUser;
