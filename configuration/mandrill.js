var mandrillConfig = {};
mandrillConfig.privateKey = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for email to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'qFYDnON9BJn5t1x3m__NEg';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'ytQi-ar_NFjr9av8kK2IPA';
    } else if (/development/i.test(process.env.ENV)) {
        return 'ytQi-ar_NFjr9av8kK2IPA';
    }else if (/automatedTest/i.test(process.env.ENV)) {
        return 'ytQi-ar_NFjr9av8kK2IPA';
    }
};

mandrillConfig.resetPasswordSlug = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for email to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'reset-password';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'reset-password';
    } else if (/development/i.test(process.env.ENV)) {
        return 'reset-password';
    }else if (/automatedTest/i.test(process.env.ENV)) {
        return 'reset-password';
    }
};

mandrillConfig.sendExceptionEmail = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for email to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return true;
    } else if (/staging/i.test(process.env.ENV)) {
        return false;
    } else if (/development/i.test(process.env.ENV)) {
        return false;
    }else if (/automatedTest/i.test(process.env.ENV)) {
        return false;
    }
};

module.exports = exports = mandrillConfig;
