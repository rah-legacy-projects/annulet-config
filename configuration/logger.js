var winston = require('winston'),
    path = require('path');


winston.remove(winston.transports.Console);
//if (/development/.test(process.env.ENV)) {
winston.add(winston.transports.Console, {
    level: 'silly',
    colorize: true
});
winston.silly('added silly console');
//}
exports = module.exports = winston;
