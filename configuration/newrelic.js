var newrelicConfig = {},
    path = require('path');
newrelicConfig.licenseKey = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for email to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'deaf334b70c1b61dcd66d22bb01e1c05419a51bf';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'NO_NEWRELIC';
    } else if (/development/i.test(process.env.ENV)) {
        return 'NO_NEWRELIC';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'NO_NEWRELIC';
    }
};

newrelicConfig.appName = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for email to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'ANNULET_PRD';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'NO_NEWRELIC';
    } else if (/development/i.test(process.env.ENV)) {
        return 'NO_NEWRELIC';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'NO_NEWRELIC';
    }
};

//hack: set the newrelic configuration path to be the project root
//hack: this avoids problems with starting the application from pwd instead of application root
process.env.NEW_RELIC_HOME = path.resolve(__dirname);
module.exports = exports = newrelicConfig;
