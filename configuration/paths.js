var paths = {};
paths.apiUri = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'NOT SET';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'http://annulet-api.rosshinkley.com';
    } else if (/development/i.test(process.env.ENV)) {
        return 'http://api.annulet.io';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'http://localhost:5001'
    }
};
paths.authUri = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'NOT SET';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'http://annulet-id.rosshinkley.com/auth';
    } else if (/development/i.test(process.env.ENV)) {
        return 'http://auth.annulet.io/auth';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'http://localhost:5002/auth'
    }
};

paths.uiUri = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'NOT SET';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'http://annulet-ui.rosshinkley.com';
    } else if (/development/i.test(process.env.ENV)) {
        return 'http://site.annulet.io';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'http://localhost:5003'
    }
};

paths.domain = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'NOT SET';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'rosshinkley.com';
    } else if (/development/i.test(process.env.ENV)) {
        return 'annulet.io';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'localhost'
    }
};

paths.dbListing = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return require('../databaseLists/databaseList-prod.json');
    } else if (/staging/i.test(process.env.ENV)) {
        return require('../databaseLists/databaseList-staging.json');
    } else if (/development/i.test(process.env.ENV)) {
        return require('../databaseLists/databaseList-dev.json');
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return require('../databaseLists/databaseList-automatedTest.json');
    }
};

paths.authDb = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        throw new Error('not ready for prod yet');
    } else if (/staging/i.test(process.env.ENV)) {
        return 'mongodb://localhost/annulet-auth';
    } else if (/development/i.test(process.env.ENV)) {
        return 'mongodb://localhost/annulet-auth';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'mongodb://localhost/annulet-auth-test'
    }
};

paths.internalKey = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for pathing to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 'KZ6BL7JW45QVXMH39IKVO6UCPJU94';
    } else if (/staging/i.test(process.env.ENV)) {
        return 'FEE58F81A887E57C4528A5525C7E2';
    } else if (/development/i.test(process.env.ENV)) {
        return 'BFF26DE59996B73B35E6B1BCE13C3';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'AUTOTEST'
    }
};

paths.clientAppRequireConfig = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for require to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return '/client_app/require-setup.min.js';
    } else if (/staging/i.test(process.env.ENV)) {
        return '/client_app/require-setup.js';
    } else if (/development/i.test(process.env.ENV)) {
        return '/client_app/require-setup.js';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'AUTOTEST'
    }
};

paths.signupAppRequireConfig = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for require to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return '/signup_app/require-setup.min.js';
    } else if (/staging/i.test(process.env.ENV)) {
        return '/signup_app/require-setup.js';
    } else if (/development/i.test(process.env.ENV)) {
        return '/signup_app/require-setup.js';
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 'AUTOTEST'
    }
};

module.exports = exports = paths;
