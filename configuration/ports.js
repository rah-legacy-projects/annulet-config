var ports = {};
ports.apiPort = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for ports to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 4002;
    } else if (/staging/i.test(process.env.ENV)) {
        return 6998;
    } else if (/development/i.test(process.env.ENV)) {
        return 3998;
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 5001;
    }
};
ports.authPort = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for ports to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 4001;
    } else if (/staging/i.test(process.env.ENV)) {
        return 6997;
    } else if (/development/i.test(process.env.ENV)) {
        return 3997;
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 5002;
    }
};
ports.uiPort = function() {
    if (!process.env.ENV) {
        throw new Error("Environment must be defined for ports to work.");
    } else if (/production/i.test(process.env.ENV)) {
        return 4000;
    } else if (/staging/i.test(process.env.ENV)) {
        return 7000;
    } else if (/development/i.test(process.env.ENV)) {
        return 3999;
    } else if (/automatedTest/i.test(process.env.ENV)) {
        return 5003;
    }
};

module.exports = exports = ports;
